<pre>
<?php
	
	function exportToCsv($db){
		$csv = "";
		foreach ($db as $key => $value) {
			$head = join("|",array_keys($value));
			$line = join("|", $value);
			$csv .= $line . PHP_EOL;
			
		}
		print_r($db);	
		file_put_contents("db.csv", $head . PHP_EOL . $csv);
	}

	function getTableContent($content){
		$table_begin = '<table class="table table-inverse table-hover">';
		$table_end = '</table>';

		$table = $content;
		$table = substr($table, strpos($table, $table_begin), strlen($table));
		$table = substr($table, 0, strpos($table, $table_end) + strlen($table_end));

		$json = json_decode(json_encode(simplexml_load_string($table)));
				
		// head
		$head = $json->thead->tr->th;
		
		// body
		$body = [];
		foreach ($json->tbody->tr as $key => $value) {
			$body[] = [
				$head[0] => $value->td[0],
				$head[1] => $value->td[1],
				$head[2] => $value->td[2],
				$head[3] => $value->td[3],
				$head[4] => json_encode($value->td[4]),
				$head[5] => $value->td[5],
				$head[6] => json_encode($value->td[6])	
			];
		}
		return $body;
	}

	$db = [];

	for($i=1; $i<=21; $i++){
		$url = "http://www.salarii-it.ro/salarii?page=$i";
		$content = file_get_contents($url);
		$table_content = getTableContent($content);
		$db = array_merge($db, $table_content);
	}


	exportToCsv($db);

	
